FROM centos:centos7

ARG PACKER_VERSION="1.6.6"
ARG TERRAFORM_VERSION="0.12.31"
ARG ANSIBLE_VERSION="2.9.26"

ENV PACKER_VERSION=${PACKER_VERSION}
ENV TERRAFORM_VERSION=${TERRAFORM_VERSION}
ENV ANSIBLE_VERSION=${ANSIBLE_VERSION}
RUN yum update -y
RUN yum install python3 python3-setuptools unzip wget -y
RUN pip3 install ansible==${ANSIBLE_VERSION} cryptography==3.3
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN wget https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip
RUN unzip "*.zip" -d /usr/local/bin

CMD ["/bin/bash"]